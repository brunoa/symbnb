<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountFormType;
use App\Entity\PasswordUpdate;
use App\Form\RegistrationFormType;
use App\Form\PasswordUpdateFormType;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * On fait intervenir ici :
 * - le security yml qui va gerer les login/logout et saura où aller chercher les users (le provider)
 * - La classe AuthenticationUtils qui nous permettra de catrch les erreurs
 */
class AccountController extends AbstractController
{
    /**
     * Symfony reconnait la fonction login et la gere seul grace à son identification dasn le SECURITY.yml
     * 
     * @Route("/login", name="account_login")
     * 
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $username = $authenticationUtils->getLastUsername();
        return $this->render('account/login.html.twig', [
            'hasError' => $error !== null,
            'username' => $username
        ]);
    }

    /**
     * Pour se déconnecter
     *
     * @Route("/logout", name="account_logout")
     * @return void
     */
    public function logout()
    {
    }

    /**
     * Affichage du form d'inscription
     * 
     * @Route("/register", name="account_register")
     *
     * @return Response
     */
    public function register(Request $request, EntityManagerInterface $entityManagerInterface, UserPasswordEncoderInterface $passwordEncoderInterface)
    {
        $user = new User();

        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // le mot de passe est verifie AVANT hashage, dans le isValid()
            $hash = $passwordEncoderInterface->encodePassword($user, $user->getHash());
            $user->setHash($hash);
            $entityManagerInterface->persist($user);
            $entityManagerInterface->flush();

            $this->addFlash(
                'success',
                "Votre compte a bien été créé !"
            );
            return $this->redirectToRoute("account_login");
        }

        return $this->render('account/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Affichage du form de modif du profil
     * 
     * Grace à : $user = $this->getUser();
     * le formulaire sera pré-rempli !
     * 
     * @Route("/account/profile",name="account_profile")
     * 
     * @IsGranted("ROLE_USER")
     *
     * @return void
     */
    public function profile(Request $request, EntityManagerInterface $entityManagerInterface)
    {
        $user = $this->getUser(); // dans un CONTROLLER, cette fonction donne acces au user connecté
        $form = $this->createForm(AccountFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($user);
            $entityManagerInterface->flush();

            $this->addFlash(
                'success',
                "Profil modifié avec succès !"
            );
        }


        return $this->render('account/profile.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * Modif du mot de passe
     * 
     * @Route("/account/password-update",name="account_password")
     * 
     * @IsGranted("ROLE_USER")
     * 
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $passwordEncoderInterface, EntityManagerInterface $entityManagerInterface)
    {
        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();
        $form = $this->createForm(PasswordUpdateFormType::class, $passwordUpdate);
        $form->handleRequest($request);
        // le isValid() reprend toutes les validations mises en annotation dan sle classe (les Assert\Equals etc...)
        if($form->isSubmitted() && $form->isValid()){
            // 1- Vérifier que l'ancien password est bien le bon (pas pris en charge par le validator)
            if(!password_verify($passwordUpdate->getOldPassword(), $user->getHash())){
                // gérer l'erreur
                $form->get('oldPassword') //donne acces au 'sous-formulaire' de la classe : le champ oldPassword (qui est lui aussi un form !)
                ->addError(new FormError("Le mot de passe saisi est incorrect !")); // et on peut ajouter une erreur à n'importe quel form !
            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $passwordEncoderInterface->encodePassword($user, $newPassword);
                $user->setHash($hash);

                $entityManagerInterface->persist($user);
                $entityManagerInterface->flush();

                $this->addFlash(
                    'success',
                    'Votre mot de pass a bien été mis à jour !'
                );

                $this->redirectToRoute('home');

            }
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher le profil du user connecté
     * 
     * @Route("/account", name="account_index")
     * 
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function myAccount()
    {
       return $this->render('user/index.html.twig', [
            'user' => $this->getUser() // Magie, Symfony renvoie le user connecté !
        ]);
    }

    /**
     * Permet d'affichher la liste des résa faites par l'utilisateur
     * 
     * @Route("/account/bookings", name="account_bookings")
     *
     * @return void
     */
    public function bookings()
    {
        return $this->render('account/bookings.html.twig');
    }
}
