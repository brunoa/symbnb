<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Image;
use App\Form\AdFormType;
use App\Repository\AdRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    /**
     * Pour afficher la liste des annonces
     * 
     * @Route("/ads", name="ad_index")
     * 
     * @return Response
     */
    public function index(AdRepository $adRepository): Response
    {
        // $repo = $this->getDoctrine()->getRepository(Ad::class); // remplacé par l'injection de dépendance
        $ads = $adRepository->findAll();

        return $this->render('ad/index.html.twig', [
            'ads' => $ads,
        ]);
    }

    /**
     * Formulaire de création d'une annonce
     * 
     * @Route("/ads/new", name="ads_create")
     * 
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $manager): Response
    {
        // Cette méthode DOIT se trouver AVANT le show() car les routes sont interprétées les unes après les autres
        // Si on laisse show() avant le create(), on aura une erreur.
        $ad = new Ad();

        // Tout ce qui suit est remplacé par :
        $form = $this->createForm(AdFormType::class, $ad);
        // fonctionne car on a créé entre temps le AdFormType qui regroupe les détails

        // Prise en compte de la requete :
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // gestion des objets image:
            foreach ($ad->getImages() as $image) {
                $image->setAd($ad);
                $manager->persist($image);
            }

            $ad->setAuthor($this->getUser());
            // $manager = $this->getDoctrine()->getManager(); // finalement, passons le en injection de dépendance !
            $manager->persist($ad);
            $manager->flush();

            // On va ici coller un message flash:
            $this->addFlash(
                'success', // tester en mettant un nom arbitraire (est ce un nom de classe bootstrap?)
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été enregistrée :)"
            );
            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug(),
            ]);
        }

        return $this->render('ad/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher le formulaire d'édition
     *
     * @Route("/ads/{slug}/edit", name="ads_edit")
     * 
     * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()", 
     * message="Ceci n'est pas votre annonce, n'imaginez même pas la modifier !!!")
     * 
     * @return Response
     */
    public function edit(Ad $ad, Request $request, EntityManagerInterface $manager)
    {

        $form = $this->createForm(AdFormType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // gestion des objets image:
            foreach ($ad->getImages() as $image) {
                $image->setAd($ad);
                $manager->persist($image);
            }

            // $manager = $this->getDoctrine()->getManager(); // finalement, passons le en injection de dépendance !
            $manager->persist($ad);
            $manager->flush();

            // On va ici coller un message flash:
            $this->addFlash(
                'success', // tester en mettant un nom arbitraire (est ce un nom de classe bootstrap?)
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été mise à jour :)"
            );
            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug(),
            ]);
        }

        return $this->render('ad/edit.html.twig', [
            'form' => $form->createView(),
            'ad' => $ad
        ]);
    }

    /**
     * Affiche une seule annonce
     * 
     * @Route("/ads/{slug}", name="ads_show")
     *
     * @return Response
     */
    public function show(Ad $ad)
    {
        // L'injection de dépendance Ad $ad met en jeu le PARAMCONVERTER
        // Ainsi, plus besoin de faire comme en dessous car Symfony liera automatiquement le slug à une entité Ad
        // Donc on convertit le SLUG en une ANNONCE, d'ailleurs, on va même enlever '$slug' de la signature
        // POur rappel, la signature intermédiaire était : public function show($slug, Ad $ad) {

        // public function show($slug, AdRepository $adRepository) {
        //     $ad = $adRepository->findOneBySlug($slug);

        return $this->render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    }

    /**
     * Permet de supprimer une annonce
     * 
     * @Route("/ads/{slug}/delete", name="ads_delete")
     * @Security("is_granted('ROLE_USER') and user == ad.getAuthor()", message="Ressource interdite")
     * 
     * @param Ad $ad
     * @param ObjectManager $objectManager
     * @return Response
     */
    public function delete(Ad $ad, EntityManagerInterface $manager)
    {
        $manager->remove($ad);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'annonce <strong>{$ad->getTitle()}</strong> a bien été supprimée !"
        );

        return $this->redirectToRoute("ad_index");
    }
}
