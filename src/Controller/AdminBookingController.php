<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingFormType;
use App\Form\AdminBookingFormType;
use App\Service\PaginationService;
use App\Repository\BookingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminBookingController extends AbstractController
{
    /**
     * @Route("/admin/bookings/{page<\d+>?1}", name="admin_bookings_index")
     */
    public function index($page, PaginationService $paginationService): Response
    {
        $paginationService->setEntityClass(Booking::class)
                          ->setLimit(20)
                          ->setCurrentPage($page);
        return $this->render('admin/booking/index.html.twig', [
            'pagination' => $paginationService
        ]);
    }

    /**
     * Edition d'un résa
     * 
     * @Route("/admin/bookings/{id}/edit", name="admin_bookings_edit")
     *
     * @return Response
     */
    public function edit(Booking $booking, Request $request, EntityManagerInterface $manager)
    {
        $booking->setAmount(0); // 0 étant interprété comme NULL, l'amount sera recalculé grâce à l'annotation @ORM\PreUpdate dans la classe Booking
        $form = $this->createForm(AdminBookingFormType::class, $booking); // on peut rajouter un tableau de groupes de validation si on veut !

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($booking); //pas obligatoire pour une MAJ ! on pourrait seulmeent flush
            $manager->flush();

            $this->addFlash(
                'success',
                "Booking bien mis à jour"
            );

            return $this->redirectToRoute("admin_bookings_index");
        }

        return $this->render('admin/booking/edit.html.twig', [
            'form' => $form->createView(),
            'booking' => $booking
        ]);
    }

    /**
     * supprimer une réservation
     *
     * @Route("/admin/bookings/{id}/delete", name="admin_bookings_delete")
     * 
     * @return void
     */
    public function delete(Booking $booking, EntityManagerInterface $manager)
    {
        $manager->remove($booking);
        $manager->flush();

        $this->addFlash(
            'success',
            "La réservation <strong>{$booking->getBooker()->getFullName()}</strong> a bien été supprimé !"
        );
       return $this->redirectToRoute('admin_bookings_index');
    }
}
