<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\User;
use App\Entity\Booking;
use App\Entity\Comment;
use App\Form\BookingFormType;
use App\Form\CommentFormType;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingController extends AbstractController
{
    /**
     * @Route("/ads/{slug}/book", name="booking_create")
     * 
     * @IsGranted("ROLE_USER")
     * 
     */
    public function book(Ad $ad, Request $request, EntityManagerInterface $manager): Response
    {
        $booking = new Booking();
        $user = new User();
        $user = $this->getUser();

        // création du form...
        $form = $this->createForm(BookingFormType::class, $booking);

        // gestion du form et de la requête générée...
        $form->handleRequest($request);

        // dd($user);
        // On check le form retourné...
        if ($form->isSubmitted() && $form->isValid()) {
            // Afin de récup des infos pour fournir notre ojet, on choppe le user connecté:
            $booking->setBooker($user)
                    ->setAd($ad);

            // si dates pas dispo : msg erreur, sinon on persiste !
            if (!$booking->isBookableDates()) {
                $this->addFlash(
                    "warning",
                    "Les dates sélectionnées ne sont pas disponibles !"
                );
            } else {

                // On rajoutera dans le prototype de la fonction : l'entity manager
                $manager->persist($booking);

                $manager->flush();

                return $this->redirectToRoute('booking_show', [
                    'id' => $booking->getId(),
                    'withAlert' => true // withAlert n'éatnt pas déclaré dans les routes, il s'affichera comme un get au bout de l'url: ?withAlert=true
                ]);
            }
        }



        return $this->render('booking/book.html.twig', [
            'ad' => $ad,
            'form' => $form->createView()
        ]);
    }

    /**
     * Afficher page de reservation
     * 
     * @Route("/booking/{id}", name="booking_show")
     *
     * @param Booking $booking
     * @return Response
     */
    public function show(Booking $booking, Request $request, EntityManagerInterface $manager)
    {
        $comment = new Comment();

        $form = $this->createForm(CommentFormType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setAd($booking->getAd())
                    ->setAuthor($this->getUser());
            
            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre commentaire a bien été pris en compte !"
            );
        }

        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }
}
