<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\AdminCommentFormType;
use App\Repository\CommentRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    /**
     * Permet d'afficher tous les commentaires
     * 
     * @Route("/admin/comments/{page<\d+>?1}", name="admin_comments_index")
     */
    public function index(PaginationService $paginationService, $page): Response
    {

        $paginationService->setEntityClass(Comment::class)
                          ->setCurrentPage($page);

        return $this->render('admin/comment/index.html.twig', [
            'pagination' => $paginationService
        ]);
    }

    /**
     * Permet d'éditer un commentaire
     * 
     * @Route("/admin/comments/{id}/edit", name="admin_comments_edit")
     *
     * @param Comment 
     * @return void
     */
    public function edit(Comment $comment, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(AdminCommentFormType::class, $comment);

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            // dd($form);
            $manager->persist($comment);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "Le commentaire n° {$comment->getId()} a bien été modifié."
            );
            return $this->redirectToRoute('admin_comments_index');
        }


        return $this->render('admin/comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet de supprimer un comment
     * 
     * @Route("/admin/comment/{id}/delete", name="admin_comments_delete")
     *
     * @param Comment $comment
     * @return void
     */
    public function delete(Comment $comment, EntityManagerInterface $manager)
    {
        $manager->remove($comment);
        $manager->flush();

        $this->addFlash(
            'success',
            "Commentaire <strong>{$comment->getAuthor()->getFullName()}</strong> a bien été supprimé !"
        );
       return $this->redirectToRoute('admin_comments_index');
    }
}
