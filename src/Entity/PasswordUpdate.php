<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PasswordUpdateRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * On supprime toutes les annotations ORM
 * Cette entité ne doit pas etre liee a la bdd,  on la veut pour avoir acces aux validators de symfony
 */
class PasswordUpdate
{
 
    private $id;

    
    private $oldPassword;

    /**
     * @Assert\Length(min=8,minMessage="Votre mot de passe doit fairre au moins 8 caractères")
     *
     */
    private $newPassword;

    /**
     * @Assert\EqualTo(propertyPath="newPassword", message="Les mots de passe ne correspondent pas !")
     *
     */
    private $confirmPassword;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): self
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    public function setNewPassword(string $newPassword): self
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }
}
