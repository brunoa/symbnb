<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

class PaginationService
{

    private $entityClass; // représentera l'entité sur laquelle on veut faire de la pagination
    private $limit = 10;
    private $currentPage = 1;
    private $manager;
    private $twig;
    private $route;
    private $templatePath;

    public function __construct(EntityManagerInterface $manager, Environment $twig, RequestStack $requestStack, $templatePath)
    {
        $this->manager  = $manager;
        $this->twig     = $twig;
        $this->route    = $requestStack->getCurrentRequest()->attributes->get('_route');
        $this->templatePath = $templatePath;

    }

    /**
     * Permet de créer le flux html à afficher !
     * 
     * La variable $templatePath permettra de pouvoir modifier le chemin vers les templates
     * Comment gérer cela ? Dans le ficheir SERVICE.YML !!!
     * 
     * On retournera les variables utilisées dans la template twig de pagination
     *
     * @return void
     */
    public function display()
    {
        // $this->twig->display('admin/partials/pagination.html.twig', [
        $this->twig->display($this->templatePath, [
            'page'  => $this->currentPage,
            'pages' => $this->getPages(),
            'route' => $this->route
        ]);
    }

    /**
     * Fonction clé du service, renvoi les tableaux d'objets !
     *
     * @param Type $var
     * @return void
     */
    public function getData()
    {
        // 1-Calculer offset
        $offset = $this->currentPage * $this->limit - $this->limit;

        // 2-Chopper le bon repository
        $repo = $this->manager->getRepository($this->entityClass);
        $data = $repo->findBy([], [], $this->limit, $offset);

        // 3-Renvoyer les éléments
        return $data;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPages()
    {
        // 1- total des enregistrements
        $repo = $this->manager->getRepository($this->entityClass);
        $total = count($repo->findAll());

        // 2- faire la division pour le npmbre de pages
        $pages = ceil($total / $this->limit);

        return $pages;
    }

    /**
     * Get the value of entityClass
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Set the value of entityClass
     *
     * @return  self
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * Get the value of limit
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set the value of limit
     *
     * @return  self
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get the value of currentPage
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * Set the value of currentPage
     *
     * @return  self
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;

        return $this;
    }

    /**
     * Get the value of route
     */ 
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set the value of route
     *
     * @return  self
     */ 
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get the value of templatePath
     */ 
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Set the value of templatePath
     *
     * @return  self
     */ 
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;

        return $this;
    }
}
