<?php
namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FrenchToDateTimeTransformer implements DataTransformerInterface {
    public function transform($date)
    {
        if ($date === null) {
           return '';
        }
        return $date->format('d/m/Y');
    }

    public function reverseTransform($frenchDate)
    {
        # on attend une date du genre 21/09/1992
        if ($frenchDate === null) {
            # Exception (message destiné au debug mais PAS SUR LE FRONT !)
            throw new TransformationFailedException("Vous devez fournir une date !", 1);
            
        }

        $date = \DateTime::createFromFormat('d/m/Y', $frenchDate);

        if ($date === false) {
            # Exception
            throw new TransformationFailedException("Mauvais format de date", 1);
            
        }

        return $date;
    }

}