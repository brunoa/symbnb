<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;

/**
 * On a créé cette classe pour factoriser le code de configuration des champs de formulaires
 */
class ApplicationFormType extends AbstractType {
     /**
     * Permet d'avoir la configuration d'un champ
     * Par défaut, le label et le placeholder, puis
     * un troisieme argument optionnel permettra d'ajouter tous les attributs voulus
     *
     * @param [type] $label
     * @param [type] $placeholder
     * @param array $options
     * @return array
     */
    protected function getConfiguration(String $label, String $placeholder,array $options = [])
    {
        return array_merge_recursive([
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder
            ]
            ], $options);
    }
}