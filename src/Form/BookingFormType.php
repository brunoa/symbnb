<?php

namespace App\Form;

use App\Entity\Booking;
use App\Form\ApplicationFormType;
use App\Form\DataTransformer\FrenchToDateTimeTransformer;
use phpDocumentor\Reflection\PseudoTypes\False_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BookingFormType extends ApplicationFormType
{

    private $transformer;
    public function __construct(FrenchToDateTimeTransformer $frenchToDateTimeTransformer)
    {
        $this->transformer = $frenchToDateTimeTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Rappel : getConfiguration vient du ApplicationFormType perso pour se faciliter la création des champs du form
        $builder
            // Avec l'arrivée du datepicker affichant les dates déjà prises, on change deschoses....
            // ->add('startDate', DateType::class, $this->getConfiguration(
            //     "Date d'arrivée",
            //     "La date à laquelle vous arrivez",
            //     [
            //         "widget" => "single_text", // widget est une méthode prédéfinie de DateType pour une belle présentation de la date;)
            //     ]
            // ))
            ->add('startDate', TextType::class, $this->getConfiguration(
                "Date d'arrivée",
                "La date à laquelle vous arrivez"
            ))

            ->add('endDate', TextType::class, $this->getConfiguration(
                "Date de départ",
                "Date à laquelle vous décampez !"
            ))
            ->add(
                'comment',
                TextareaType::class,
                [
                    "label" => false, // ici, le label refuse de se masquer avec la fonction getConfiguration du coup je fais de manère classique
                    "attr" => [
                        "required" => false,
                        "placeholder" => "Laissez ici un commentaire..."

                    ]
                ],
            );

        // On ajoute notre transformer pour remettre nos dates string en DATE:
        $builder->get('startDate')->addModelTransformer($this->transformer);
        $builder->get('endDate')->addModelTransformer($this->transformer);
    }
    /**
     * Ici on a rajouté le tableau validation_groups. voir dan sle readme les autres possibilités
     *
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'validation_groups' => [
                'Default',
                'front'
            ]
        ]);
    }
}
