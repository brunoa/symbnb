<?php

namespace App\Form;

use App\Entity\Ad;
use App\Form\ApplicationFormType;
use Doctrine\DBAL\Types\ArrayType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdFormType extends ApplicationFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // pour ne pas répéter à chaque fois la personnalisation d'un champ, on va creer une fonction getConfiguration.
        $builder
            ->add(
                'title',
                TextType::class,
                $this->getConfiguration('Titre', 'Tapez votre titre ici !')
            )
            ->add(
                'slug',
                TextType::class,
                $this->getConfiguration('Adresse web', 'Tapez ici l\'adresse web(automatique)', ['required' => false])
            )
            ->add(
                'coverImage',
                UrlType::class,
                $this->getConfiguration('URL de l\'image', 'Donnez le lien vers l\'image')
            )
            ->add(
                'introduction',
                TextType::class,
                $this->getConfiguration('Introduction', 'Donnez une description courte de l\'annonce')
            )
            ->add(
                'content',
                TextareaType::class,
                $this->getConfiguration('Contenu de l\'annonce', 'Description complète de l\'annonce')
            )
            ->add(
                'rooms',
                IntegerType::class,
                $this->getConfiguration('Nombre de chambres', 'Le nombre de chambres disponibles')
            )
            ->add(
                'price',
                MoneyType::class,
                $this->getConfiguration('Prix par nuit', 'Donnez votre prix par nuit !')
            )
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => ImageFormType::class,
                    'allow_add'  => true, // pour pouvoir rajouter des champs dynamiquement
                    'allow_delete' => true // pour pouvoir supprimer des images !
                ]

      )
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
