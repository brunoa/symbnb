# Symfony 5

## Twig
exemple de lien avec la fonction path():
```
<a href="{{ path('nom_route', {'var1':valeur1, 'var2':valeur2}) }}">Cliquez!</a>
```
<hr>

## Fixtures
```
symfony console make:fixtures

symfony console doctrine:fixtures:load
```
<hr>

## ParamConverter
Il fait le lien entre :
- les attributs dans les ```@Route```, 
- les attributs de la méthode correspondante, et 
- le ```$manager```
<hr>

## Groupes de validation

Ils permettent de n'appliquer certaines validation que dans certains cas.
Ainsi, on peut avoir pour une entité donnée plusieurs form avec chacun des validations différentes.

Exemple : une entité sur le front et en admin peut avoir des règles de validation différentes

Il convient de mettre en place 2 choses :
<br>
<br>
- Dans les commentaires d'un attribut de classe, on peut mettre :

     * @Assert\GreaterThan("today", message="La date d'arrivée doit être supérieure à aujourd'hui !", groups={"front", "autreGroupe", "encoreUnGroupe"})

Ainsi, cette validation n'est appliquée qu'au groupe(s) spécifié(s).
Par défaut, sans précision <i>groups</i>, la validation est appliquée en permanence.
<br>
- Dans le controller qui appelle a créer le form, on précisera quel(s) <i>groups</i> utiliser : 

        $form->createForm(MonObjetFormType::class, $monObjet, [
            'validation_groups' => [ "front" ]
        ]);

### Compréhension:
Il exste des groupes de validation par défaut : <i>Default</i> est le groupe implicite auquel toutes les validations sont soumises.
<br>
<br>
<u>DONC POUR NE PAS APPLIQUER</u> une certaine validation, il conviendra de:
- écrire dans l'ANNOTATION D'ASSERTION de l'attribut de classe le nom d'un groupe
- ne pas mettre ce groupe de validation dans le tableau du controller qui NE DOIT PAS recevoir cette validation ! (mettre "Default" ou ne pas mettre de tableau du tout !)
- Enfin, IMPORTANT ! Mettre à jour le tableau de groupes dans un autre controller qui, lui, du coup vient de perdre la validation !

### Edit :
Pour garder nos controleurs propres, on passera le tableau de validators via la fonction <i>configureOptions</i> définie le formType ! (voir dans <i>BookingFormType.php</i>)
<hr>

## Pagination et Repositories
La pagination se fait depuis la fonction <i>findBy()</i> d'un repository.
La fonction <i>findBy()</i> prend 4 paramètres :

        $objet = $repo->findBy([], [], 5, 0);

1- [] : Un tableau de valeurs d'attributs (des AND).
ex: 

        ['title' => 'Super Titre',
        'annee' => 1992 ]

2- [] : Un tableau de tri.
ex:

        [

        ]
3- Le nombre d'enregistrements à retourner

4- L'id à partir duquel commence la recherche

Pour faire des jointures, des OR, XOR etc... Il faut passer à du DQL. 
<hr>

## Contraintes et requirements
Dans l'annotation de la route il est possible de placer des contraintes de 2 manières :

1- 

        * @Route("/admin/ads/{page}", name="admin_ads_index", requirements={"page":"\d+"})
Une regExp oblige a saisir un ou plusieurs digits, OBLIGATOIREMENT. requirements est un TABLEAU qui peut donc contenir plusieurs contraintes.

OU

2-

        * @Route("/admin/ads/{page<\d+>?}", name="admin_ads_index")
Grâce au <i>?</i>, le digit est rendu optionnel mais la valeur par défaut renvoyée est 0

        * @Route("/admin/ads/{page<\d+>?1}", name="admin_ads_index")
Grâce au <i>?</i>, le digit est rendu optionnel et on choisit la valeur par défaut !
<hr>

## Pagination et Service

Dans les entités, les fonctions respectant l'écriture : 
        class Truc {
            getMachin()
        }
Qu'il s'agisse ou pas d'un attibut (donc directement une fonction), sera décodé dans TWIG comme suit :

```twig
        {{ truc.machin }}
```
<hr>

## RequestStack
On s'amuse à remplacer dans les twigs les includes de partials par des méthodes de classe.
ex : 

		{% include "admin/partials/pagination.html.twig" with {'route': 'admin_ads_index'} %}

devient :

                {% pagination.render() %}

Liste des injections possibles dans les services de Symfony :

                symfony console debug:container

La RequestStack est un service qui permet de connaitre la requete en cours.
On l'utilise dasn le service : <i>PaginationService.php</i> en injection de dépendance.
Il permet de récupérer le nom de la route en cours et permet de rendre le service encore un peu plus autonome.
<br>
C'est l'injection de l'objet <i>Environment $twig</i> qui permet de disposer des méthodes display() et render().