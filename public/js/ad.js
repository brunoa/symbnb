$('#add-image').click(function () {
    // la taille du tableau est l'index suivant !
    // const index = $('#ad_form_images div.form-group').length;
    // correction du bug ( voir le haur du fichier)
    // le + caste en int
    const index = + $('#widgets-counter').val();

    // récupération du code et mise en place de l'index !
    const tmpl = $('#ad_form_images').data('prototype').replace(/__name__/g, index);

    // Ajout du code dans la page
    $('#ad_form_images').append(tmpl);

    $('#widgets-counter').val(index + 1)

    // Appel de la fonction delete:
    handleDeleteButtons();
})

function handleDeleteButtons() {
    $('button[data-action="delete"]').click(function () {
    const target = this.dataset.target;
    $(target).remove()
     });
}

function updateCounter(){
    const count = +$('#ad_form_images div.form-group').length;

    $('#widgets-counter').val(count);
}

    // Appelée au chargement pour l'affichage d'annonces qui ont déjà des images !
    handleDeleteButtons();
    updateCounter()